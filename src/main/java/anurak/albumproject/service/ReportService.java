
package anurak.albumproject.service;

import java.util.List;

import anurak.albumproject.dao.SaleDao;
import anurak.albumproject.model.ReportSale;

public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
